//
//  Song+CoreDataProperties.swift
//  
//
//  Created by Akshay  Chavan on 09/01/21.
//
//

import Foundation
import CoreData


extension Song {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Song> {
        return NSFetchRequest<Song>(entityName: "Song")
    }

    @NSManaged public var name: String?
    @NSManaged public var artist: String?
    @NSManaged public var imageUrl55: String?
    @NSManaged public var imageUrl170: String?
    @NSManaged public var price: String?
    @NSManaged public var releaseDate: String?
    @NSManaged public var rights: String?
    @NSManaged public var songUrl: String?

}
