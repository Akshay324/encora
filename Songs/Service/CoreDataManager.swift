//
//  CoreDataService.swift
//  Songs
//
//  Created by Akshay  Chavan on 09/01/21.
//

import Foundation
import CoreData
import UIKit

class CoreDataManager {
    
    static let sharedManager = CoreDataManager()
    private var appDelegate:AppDelegate?
    
    private init() {
        DispatchQueue.main.async {
            self.appDelegate = UIApplication.shared.delegate as? AppDelegate
        }
    }
    
    func getManagedObject(for entity:String) -> NSManagedObject? {
        var entityObj:NSManagedObject?
        guard let context = appDelegate?.persistentContainer.viewContext else{return nil}
            let entity = NSEntityDescription.entity(forEntityName: entity, in: context)
            entityObj = NSManagedObject(entity: entity!, insertInto: context)
        return entityObj
    }
    
    func saveObject(object:NSManagedObject){
            guard let context = self.appDelegate?.persistentContainer.viewContext else{return}
            do {
                try context.save()
            } catch {
                print("Failed saving")
            }
    }
    
    func fetchAllObjects(from entity:String,completionHandle:@escaping([NSManagedObject])->Void){
        let request = NSFetchRequest<NSFetchRequestResult>(entityName:entity)
        //request.predicate = NSPredicate(format: "age = %@", "12")
        request.returnsObjectsAsFaults = false
        if self.appDelegate == nil {
            self.appDelegate = UIApplication.shared.delegate as? AppDelegate
        }
        guard let context = self.appDelegate?.persistentContainer.viewContext else{return}

        do {
            let result = try context.fetch(request)
            completionHandle(result as! [NSManagedObject])
        } catch {
            print("Failed")
        }
    }
}
