//
//  DocumentDirectoryManager.swift
//  Songs
//
//  Created by Akshay  Chavan on 10/01/21.
//

import Foundation
import UIKit

class DocumentDirectoryManager {
    
    func saveImageInDocumentDirectory(from url:String, image:UIImage?){
        let documentsURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
        let array = url.components(separatedBy: "/")
        let finalImageName = "\(array[array.count-2])/\(array[array.count-1])"
        let fileURL = documentsURL.appendingPathComponent(finalImageName)
        
        if let data = image?.pngData(){
            try? data.write(to: fileURL)
        }
    }
    
    func getImageFromDocumentDirectory(for url:String)->UIImage?{
        let documentsURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
        let array = url.components(separatedBy: "/")
        let finalImageName = "\(array[array.count-2])/\(array[array.count-1])"
        let fileURL = documentsURL.appendingPathComponent(finalImageName)
        let image = UIImage(contentsOfFile: fileURL.path)
        return image
    }
    
    
    func saveAudioInDocumentDirectory(from url:String){
        
        if let audioUrl = URL(string: url) {
            
            // then lets create your document folder url
            let documentsDirectoryURL =  FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
            
            // lets create your destination file url
            let destinationUrl = documentsDirectoryURL.appendingPathComponent(audioUrl.lastPathComponent)
            print(destinationUrl)
            
            // to check if it exists before downloading it
            if FileManager.default.fileExists(atPath: destinationUrl.path) {
                print("The file already exists at path")
                
                // if the file doesn't exist
            } else {
                
                // you can use NSURLSession.sharedSession to download the data asynchronously
                URLSession.shared.downloadTask(with: audioUrl, completionHandler: { (location, response, error) -> Void in
                    guard let location = location, error == nil else { return }
                    do {
                        // after downloading your file you need to move it to your destination url
                        try FileManager.default.moveItem(at: location, to: destinationUrl)
                        print("File moved to documents folder")
                    } catch let error as NSError {
                        print(error.localizedDescription)
                    }
                }).resume()
            }
        }
    }
    
    
}
