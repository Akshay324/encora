//
//  ServiceManager.swift
//  Songs
//
//  Created by Akshay  Chavan on 09/01/21.
//

import Foundation
import SwiftyXMLParser
import CoreData

extension Error {
    var code: Int { return (self as NSError).code }
    var domain: String { return (self as NSError).domain }
}

class ServiceManger:NSObject {    
    func download<T:NSManagedObject>(url:String,completionHandler:@escaping (Result<[T],NetworkError>)->Void){
        let task = URLSession.shared.dataTask(with: NSURL(string: url)! as URL, completionHandler: { (data, response, error) -> Void in
            
            if error != nil {
                switch error?.code {
                    case 400:
                        completionHandler(.failure(.BadRequest))
                    case 404:
                        completionHandler(.failure(.NotFound))
                    case 408:
                        completionHandler(.failure(.RequestTimeout))
                    case 500:
                        completionHandler(.failure(.InternalServerError))
                    case 503:
                        completionHandler(.failure(.ServiceNotAvailable))
                    case -1009:
                        completionHandler(.failure(.Offline))
                    default:
                        completionHandler(.failure(.unknownError))
                }
            }
            
            var songsArray:[T] = []
            
            if let xmlData = data {
                let xml = XML.parse(xmlData)
                if case .failure(XMLError.interruptedParseError) = xml {
                    completionHandler(.failure(.InterruptedParseError))
                }else{
                    let element = xml.feed.entry
                    for obj in element {
                        
                        let name = obj.title.text
                        let artist = obj["im:artist"].text
                        let imageUrl55 = obj["im:image"][0].text
                        ServiceManger.downloadImage(for: imageUrl55 ?? "")
                        let imageUrl170 = obj["im:image"][2].text
                        ServiceManger.downloadImage(for: imageUrl170 ?? "")

                        let price = obj["im:price"].text
                        let releaseDate = obj["im:releaseDate"].attributes["label"]
                        let songUrl = obj["link"][1].attributes["href"]
                        DocumentDirectoryManager().saveAudioInDocumentDirectory(from: songUrl ?? "")
                        let rights = obj["rights"].text
                        
                        
                        if let song  = CoreDataManager.sharedManager.getManagedObject(for: "Song"){
                            song.setValue(name, forKey: "name")
                            song.setValue(artist, forKey: "artist")
                            song.setValue(imageUrl55, forKey: "imageUrl55")
                            song.setValue(imageUrl170, forKey: "imageUrl170")
                            song.setValue(price, forKey: "price")
                            song.setValue(releaseDate, forKey: "releaseDate")
                            song.setValue(songUrl, forKey: "songUrl")
                            song.setValue(rights, forKey: "rights")
                            CoreDataManager.sharedManager.saveObject(object: song)
                            songsArray.append(song as! T)
                        }
                    }
                    completionHandler(.success(songsArray))
                }
                
            }
        })
        task.resume()
    }
    
   static private func downloadImage(for link:String){
        URLSession.shared.dataTask( with: NSURL(string:link)! as URL, completionHandler: {
            (data, response, error) -> Void in
            let directoryManager = DocumentDirectoryManager()
            guard let imageData = data else {return}
            directoryManager.saveImageInDocumentDirectory(from: link, image: UIImage(data: imageData))
        }).resume()
    }
    
}
