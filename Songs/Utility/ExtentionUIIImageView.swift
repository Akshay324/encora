//
//  ExtentionUIIImageView.swift
//  Songs
//
//  Created by Akshay  Chavan on 10/01/21.
//

import UIKit

extension UIImageView {
    func downloadImageFrom(link:String, contentMode: UIView.ContentMode) {
        let directoryManager = DocumentDirectoryManager()
        if let image = directoryManager.getImageFromDocumentDirectory(for: link) {
            DispatchQueue.main.async {
                self.contentMode =  contentMode
                self.image = image
            }
        }else{
            URLSession.shared.dataTask( with: NSURL(string:link)! as URL, completionHandler: {
                (data, response, error) -> Void in
                DispatchQueue.main.async {
                    self.contentMode =  contentMode
                    if let data = data { self.image = UIImage(data: data) }
                    directoryManager.saveImageInDocumentDirectory(from: link, image: self.image)
                }
            }).resume()
        }
    }
}
