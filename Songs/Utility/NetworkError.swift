//
//  NetworkError.swift
//  Songs
//
//  Created by Akshay  Chavan on 09/01/21.
//

import Foundation

enum NetworkError:Error {
    case BadRequest
    case NotFound
    case RequestTimeout
    case InternalServerError
    case ServiceNotAvailable
    case InterruptedParseError
    case unknownError
    case Offline
}
