//
//  SongDetailViewController.swift
//  Songs
//
//  Created by Akshay  Chavan on 09/01/21.
//

import UIKit
import AVFoundation
import CoreData

class SongDetailViewController: UIViewController {
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var songNameLabel: UILabel!
    @IBOutlet weak var artistLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var releaseDateLabel: UILabel!
    @IBOutlet weak var rightsLabel: UILabel!
    var song:NSManagedObject?
    var audioPlayer : AVAudioPlayer!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        imageView.downloadImageFrom(link: song?.value(forKey: "imageUrl170") as? String ?? "", contentMode: .scaleToFill)
        songNameLabel.text = song?.value(forKey: "name") as? String ?? ""
        artistLabel.text = "Artist : \(song?.value(forKey: "artist") as? String ?? "")"
        priceLabel.text = "Price : \(song?.value(forKey: "price") as? String ?? "")"
        releaseDateLabel.text = "Release Date : \(song?.value(forKey: "releaseDate") as? String ?? "")"
        rightsLabel.text = song?.value(forKey: "rights") as? String ?? ""
    }
    
    @IBAction func playButtonTapped(_ sender: Any) {
        if let string = song?.value(forKey: "songUrl") as? String {
            playAudioFromDocumentDirectory(from: string)
        }
    }
    
    func playAudioFromDocumentDirectory(from url:String) {
        
        if let audioUrl = URL(string: url) {
            
            // then lets create your document folder url
            let documentsDirectoryURL =  FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
            
            // lets create your destination file url
            let destinationUrl = documentsDirectoryURL.appendingPathComponent(audioUrl.lastPathComponent)
            
            //let url = Bundle.main.url(forResource: destinationUrl, withExtension: "mp3")!
            
            do {
                audioPlayer = try AVAudioPlayer(contentsOf: destinationUrl)
                guard let player = audioPlayer else { return }
                
                player.prepareToPlay()
                player.play()
            } catch let error {
                print(error.localizedDescription)
            }
        }
        
    }

}

