//
//  SongsViewController.swift
//  Songs
//
//  Created by Akshay  Chavan on 09/01/21.
//

import UIKit
import CoreData

class SongsViewController: UIViewController {
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet var errorLabel: UILabel!
    
    var viewModel:SongsProtocol = SongsViewModel()
    override func viewDidLoad() {
        viewModel.delegate = self
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
}


extension SongsViewController:UITableViewDelegate,UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        viewModel.songs?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CELL")
        if let song = viewModel.songs?[indexPath.row] {
            cell?.textLabel?.numberOfLines = 0
            cell?.textLabel?.text = song.value(forKey: "name") as? String
            cell?.imageView?.downloadImageFrom(link: song.value(forKey: "imageUrl55") as? String ?? "", contentMode: .scaleAspectFit)
        }
        
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(identifier: "SongDetailViewController") as? SongDetailViewController {
            vc.song = viewModel.songs?[indexPath.row]
            vc.edgesForExtendedLayout = []
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
}

extension SongsViewController:SongsViewModelProtocol{
    func updateUI() {
        DispatchQueue.main.async {
            self.tableView.isHidden = false
            self.tableView.reloadData()
        }
    }
    
    func didFailWithError(error: NetworkError) {
        DispatchQueue.main.async {
            switch error {
                case NetworkError.BadRequest:
                    self.errorLabel.text = "Bad Request."
                case NetworkError.InternalServerError:
                    self.errorLabel.text = "Internal Server Error."
                case NetworkError.InterruptedParseError:
                    self.errorLabel.text = "Parsing Interrupted."
                case NetworkError.NotFound:
                    self.errorLabel.text = "Not Found."
                case NetworkError.RequestTimeout:
                    self.errorLabel.text = "Request Timeout."
                case NetworkError.ServiceNotAvailable:
                    self.errorLabel.text = "Server is currently down."
                case NetworkError.unknownError:
                    self.errorLabel.text = "Something went wrong."
                case NetworkError.Offline:
                    self.errorLabel.text = "The Internet connection appears to be offline."
            }
        }
    }
    
}


