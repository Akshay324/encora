//
//  SongsViewModel.swift
//  Songs
//
//  Created by Akshay  Chavan on 09/01/21.
//

import UIKit
import CoreData

protocol SongsViewModelProtocol:NSObject {
    func updateUI()
    func didFailWithError(error:NetworkError)
}

protocol SongsProtocol {
    var songs:[NSManagedObject]?{get}
    var delegate:SongsViewModelProtocol?{get set}
}

class SongsViewModel: NSObject,SongsProtocol {
    var songs:[NSManagedObject]?
    weak var delegate:SongsViewModelProtocol?{
        didSet{
            checkForSongs()
        }
    }
    
    private func checkForSongs(){
        CoreDataManager.sharedManager.fetchAllObjects(from: "Song") {[weak self] (result) in
            if result.count > 0 {
                self?.songs = result
                self?.delegate?.updateUI()
            }else{
                self?.downloadSongs {[weak self] (songs,error) in
                    if error != nil{
                        self?.delegate?.didFailWithError(error: error!)
                    }else{
                        self?.songs = songs
                        self?.delegate?.updateUI()
                    }
                }
            }
        }
    }
    
    
    private func downloadSongs(completionHandler:@escaping(_ songs:[NSManagedObject],_ error:NetworkError?)->Void) {
        let serviceManager = ServiceManger()
        serviceManager.download(url: BASE_URL) { (result) in
            
            switch result {
                case .success(let songs):
                    completionHandler(songs,nil)
                case .failure(let error):
                    completionHandler([],error)
            }
        }
    }
}
