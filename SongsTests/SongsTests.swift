//
//  SongsTests.swift
//  SongsTests
//
//  Created by Akshay  Chavan on 09/01/21.
//

import XCTest
@testable import Songs

class SongsTests: XCTestCase {
    var serviceManager:ServiceManger!
    
    override func setUp() {
        super.setUp()
        serviceManager = ServiceManger()
    }
    
    override func tearDown() {
        serviceManager = nil
        super.tearDown()
    }
    
    // Asynchronous test: success fast, failure slow
    func testValidCallToGetSongs() {
        let promise = expectation(description: "Status code: 200")
        serviceManager.download(url: BASE_URL) { (result) in
            switch result {
                case .success( _):
                    promise.fulfill()
                    
                case .failure(let error):
                    XCTFail("Error: \(error.localizedDescription)")
            }
        }
        wait(for: [promise], timeout: 5)
    }

}
